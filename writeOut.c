/*
	! Note:
	@ Algorithm:
		writeOut():
		create file name by nid
		call inorder write.
		call preorder write.
		save and close file.
		
		inorderWrite():
		write each line as "$[nid]"

		preorderWrite():
		write each line as "[nid] [type] [slope] [freq] [exfile] [[obsid] [obspos] [obsmark]]... \n"

		postorderFree():
		free each node

*/

#include "astlib.h"

char *writeOut(STNODE *root){
	char filename[BUFSIZE];
	static FILE *fptr; // file pointer. point to the external file

	if(root->exfile == NULL){
		memset(filename, '\0', BUFSIZE);
		snprintf(filename, BUFSIZE, "%d.exf\0", root->nid);
		fptr = fopen(filename, "w+");
		if(!fptr){
			printf("[ERROR] open file: %s failed.\n", filename);
			exit(1);
		}
		// save filename into root->exfile
		root->exfile = (char *)malloc(sizeof(filename));
		//strcpy(root->exfile, filename);
		snprintf(root->exfile, sizeof(filename), "%s", filename);
	}
	else{
		fptr = fopen(root->exfile, "w+");
	}
	
	printf("\tinorderWrite");
	inorderWrite(fptr, root);
	fprintf(fptr, "\n"); // insert newline symbol at the tail of in-order nid sequence
	printf("\tpreorderWrite\n");
	preorderWrite(fptr, root, root->nid);
	
	//postorderFree(root);
	
	fclose(fptr);
	
	root->child = NULL;
	root->sibling = NULL;

}

void inorderWrite(FILE *fptr, STNODE *node){
	if(node){
		inorderWrite(fptr, node->child);
		// write the nid of the node
		fprintf(fptr, "$%d\n", node->nid);
		inorderWrite(fptr, node->sibling);
	}
}
void preorderWrite(FILE *fptr, STNODE *node, unsigned long long rnid){
	OBS *obs;
	if(node){
		// write, but don't free
		fprintf(fptr, "%d %d %d %d %s %x\n", node->nid, node->type, node->slope, node->freq, node->exfile, &node->head);
		//fprintf(fptr, "%s ", node->exfile);
		/*
		obs = node->head;
		while(obs) fprintf(fptr, "%d %d %d ", obs->id, obs->pos, obs->mark);
		*/
		//fprintf(fptr, "%x", &node->head);
		//fprintf(fptr, "\n");
		
		preorderWrite(fptr, node->child, rnid);
		preorderWrite(fptr, node->sibling, rnid);
	}
}

void postorderFree(STNODE *node){
	if(node){
		postorderFree(node->child);
		postorderFree(node->sibling);
		free(node);
		node = NULL;
	}
}
