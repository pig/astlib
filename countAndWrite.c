/*
	@ Algorithm:
		postorder trace
		each node as the root of subtree, count the node number
		if node number > NODELIMIT, then write out
*/
#include "astlib.h"

void countAndWrite(STNODE *node){
	int curNodeNum;
	if(node){
		countAndWrite(node->child);
		countAndWrite(node->sibling);
		curNodeNum = nodeCnt(node);
		printf("## Nid: %d, node number: %d\n", node->nid, curNodeNum);
		if(NODELIMIT <= curNodeNum){
			printf("\x1b[1;31m[SYSTEM] NODELIMIT(%d) <= curNodeNum(%d), execute writeOut function: %d\x1b[m\n", NODELIMIT, curNodeNum, node->nid);
			writeOut(node);
		}
	}
}
