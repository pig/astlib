#include "astlib.h"

STNODE *init(){
	STNODE *root;
	/* global variable setting */
	nid = 0;
	CRNODENUM = pow(2, 18);
	MEMUSAGE = 0;
	NODELIMIT = 9999999999999;
	/* init a root */
	root = createNode(-1, 0, -99, -1);
	return root;
}

OBS *createObs(int obs, int pos, int mark){
	OBS *node;
	node = (OBS *)malloc(sizeof(OBS));
	node->id = obs;
	node->pos = pos;
	node->mark = mark;
	node->next = NULL;
	return node;
}
STNODE *createNode(int obs, int type, int slope, int pos){
	STNODE *tnode;
	tnode = (STNODE *)malloc(sizeof(STNODE));
	tnode->nid = nid;
	tnode->type = type;
	tnode->slope = slope;
	tnode->freq = 1;
	tnode->exfile = NULL;
	tnode->head = createObs(obs, pos, 0);
	tnode->child = NULL;
	tnode->sibling = NULL;
	nid++;
	return tnode;
}
int *formated(char *temp, int *obs_id, int *slpnum){
        int cnt = 0;
        int i;
        char *tmp;
        static int slopes[SLPMAXNUM];
        // initial
        memset(slopes, -99, sizeof(slopes));
        // get the obs id
        tmp = strtok(temp, "N");
        tmp = strtok(NULL, ":");
        *obs_id = atoi(tmp);
        // save slopes into array
        while((tmp = strtok(NULL, " "))){
                slopes[cnt] = atoi(tmp);
                cnt++;
        }
        slopes[cnt] = 99;
        *slpnum = cnt;
        // DEBUG -- print all
        // for(i = 0; i <= cnt; i++) printf("slopes[%d] = %d\n", i, slopes[i]);

		return slopes;
}
int nodeCnt(STNODE *node){
	if(node) return (nodeCnt(node->child) + nodeCnt(node->sibling) + 1);
	else return 0;
}

/* functions for debug */
void slopesPrint(int *slopes){
	int i = 0;
	while(*slopes != 99){
		printf("slopes[%d] = %d\n", i, *slopes);
		slopes++;
		i++;
	}
	printf("slopes[%d] = %d\n", i, *slopes);
}
void treeTrace(STNODE *root){
	inorderPnt(root);
	printf("\n");
	preorderPnt(root);
	printf("\n");
}
void inorderPnt(STNODE *node){
	if(node){
		inorderPnt(node->child);
		printf("%d ", node->slope);
		inorderPnt(node->sibling);
	}
}
void preorderPnt(STNODE *node){
	if(node){
		preorderPnt(node->child);
		preorderPnt(node->sibling);
		printf("%d ", node->slope);
	}
}
