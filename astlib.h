#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#define SLPMAXNUM 1024
#define BUFSIZE 1024

unsigned long long nid;
double CRNODENUM;
double MEMUSAGE;
unsigned long long NODELIMIT;

typedef struct obsinfo{
	int id;
	int pos;
	int mark;
	struct obsinfo *next;
}OBS;

typedef struct node{
	unsigned long long nid; 
	int type; // 0: root, 1: internal node, 2: terminal node
	int slope; 
	int freq;
	char *exfile;
	struct obsinfo *head;
	struct node *child, *sibling;
}STNODE;

typedef struct dynamicArray{
	unsigned long long nid;
	char *other;
	int index;
	struct dynamicArray *next;
}NODEARRAY;

/* src: tools.c */
STNODE *init();
STNODE *createNode(int obs, int type, int slope, int pos);
OBS *createObs(int obs, int pos, int mark);
int *formated(char *temp, int *obs_id, int *slpnum);
int nodeCnt(STNODE *node);

/* src: insert.c */
void insert(STNODE *curnode, int obs, int *slopes, int position, int slopesLength);

/* hashTable:  */

/* src: writeOut.c */
char *writeOut(STNODE *root);
void inorderWrite(FILE *fptr, STNODE *node);
void preorderWrite(FILE *fptr, STNODE *node, unsigned long long rnid);
void postorderFree(STNODE *node);

/* src: rebuild.c */
STNODE *rebuild(char *filename);
STNODE *buildTree(NODEARRAY *inorder, NODEARRAY *preorder, int inHead, int inTail);
STNODE *pickAndCreateNode(NODEARRAY *preorder, int preIndex);
int searchIndex(NODEARRAY *inorder, int keyNid);

/* src: countAndWrite.c */
void countAndWrite(STNODE *node);

/* src: generalize.c */
void generalize(STNODE *node, int level, int custom);
int changeValue(STNODE *node, int level, int custom);
STNODE *verticalMerge(STNODE *dst, STNODE *src, STNODE *left);
STNODE *horizontalMerge(STNODE *dst, STNODE *src);
void swap(STNODE *r, STNODE *s);
void nodeInfoMerge(STNODE *dst, STNODE *src);

/* query method -- src: queryMatch.c */
void queryFullMatch(STNODE *stnode, char *query);
void queryHeadTailMatch(STNODE *stnode, int head, int tail);
void queryPartMatch(STNODE *stnode, char *query);
void markByTail(STNODE *curnode, OBS *headset, int tail);
STNODE *horizontalMerge(STNODE *dst, STNODE *src);
void swap(STNODE *r, STNODE *s);
void nodeInfoMerge(STNODE *dst, STNODE *src);

/* interface -- src: interface.c */
void interface(STNODE *root);

/* functions for debug */
void slopesPrint(int *slopes);
void treeTrace(STNODE *root);
void inorderPnt(STNODE *node);
void preorderPnt(STNODE *node);
void printAllSuffix(STNODE *node);

