/*
	# Note: 
		a. insert only work when the root exist.
		b. maybe rebuild many subtree, and result in overflow memory
	@ Agroithm:
		if curnode isn't NULL then 
				check if exfile exist or not
					if exist, rebuild
				record curnode's child
				check if child exist or not
					if it's not exist, 
						indicate there are not children. insert the current slope below this curnode.
					else, 
						indicate there are some children. find the matched node and update node's info.
						however, at last , if there are no matched children, 
						just insert current slopes at the tail of this generation.

	@ external thinking:
		when should the subtree be write out? after insert the slopes of OBS 
		how to write out a subtree? writeOut()
		how to rebuild the external tree? rebuild()

*/
#include "astlib.h"

void insert(STNODE *curnode, int obs, int *slopes, int pos, int slpnum){
	OBS *obslist;
	STNODE *chdnode, *csnode, *root;
	int mark;
	root = curnode;
	while(pos <= slpnum){
		mark = 0;
		if(curnode->exfile) rebuild(curnode->exfile);
		chdnode = curnode->child;
		if(chdnode == NULL){
			// no children, give it a child
			if(*slopes != 99) curnode->child = createNode(obs, 1, *slopes, pos);
			else curnode->child = createNode(obs, 2, *slopes, pos);
			slopes++;
			pos++;
			curnode = curnode->child;
		}
		else{
			// since there are children, need to check the children
			if(chdnode->slope == *slopes){
				/* update info.
				*/
				chdnode->freq++;
				obslist = chdnode->head;
				while(obslist->next != NULL) obslist = obslist->next;
				obslist->next = createObs(obs, pos, 0);
				/*****/
				slopes++;
				pos++;
				curnode = chdnode;
			}
			else{
				if(chdnode->sibling){
					do{
						chdnode = chdnode->sibling;
						if(chdnode->slope == *slopes){
							/* update info.
							*/
							chdnode->freq++;
							obslist = chdnode->head;
							while(obslist->next != NULL) obslist = obslist->next;
							obslist->next = createObs(obs, pos, 0);
							/*****/
							slopes++;
							pos++;
							mark = 1;
							break;
						}
					}while(chdnode->sibling);
				}
				if(!mark){
					// create a node at the tail of sibling list
					if(*slopes != 99) chdnode->sibling = createNode(obs, 1, *slopes, pos);
					else chdnode->sibling = createNode(obs, 2, *slopes, pos);
					slopes++;
					pos++;
					chdnode = chdnode->sibling;
				}
				curnode = chdnode;
			}
		}
	}
	curnode = root;
}
