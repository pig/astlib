#include "astlib.h"
#include <time.h>

// int start_time, end_time, cost_time;

int main(int argc, char *argv[]){
	int start_time, end_time, cost_time;
	start_time = time((time_t *)NULL); // record start time
	
	char filename[BUFSIZE];
	char temp[BUFSIZE];
	FILE *fptr;
	STNODE *root;
	static int *slopes, *slopes_cp;
	int flag = 0;
	int obs_id;
	int slpnum;
	int optch;
	int pos;
	int head, tail;
	int lineCnt = 0;

	/* prompt interface */
	while((optch = getopt(argc, argv, "tf:")) != -1){
		switch(optch){
			case 'f':
				// read the file
				strcpy(filename, optarg);
				printf("\x1b[1;32m# File Name: %s \x1b[m\n", filename);
				break;
			case 't':
				flag = 1;
				break;
			default:
				printf("\x1b[1;32m# [MSG] NO SUPPORT OPTION: %c\x1b[m\n", optch);
		}
	}
	/* file open */
	fptr = fopen(filename, "r");
	if(fptr == NULL){
		printf("\x1b[1;32m# [MSG] ERROR: opening file[%s] failed.\x1b[m\n", filename);
		return 0;
	}
	/* initial */
	root = init();
	/* read data from file and insert into suffix tree */
	while(fgets(temp, BUFSIZE, fptr)){
		temp[strlen(temp)-1] = '\0';
		lineCnt++;
		printf("# proc[%d]: %s\n", lineCnt, temp);
		slopes = formated(temp, &obs_id, &slpnum);
		// insert slopes of the obs into suffix tree
		for(pos = 0; pos <= slpnum; pos++){
			slopes_cp = slopes;
			insert(root, obs_id, slopes_cp, pos, slpnum);
			slopes++;
		}
		// count the usage of memory & write out when reach the limition
		if(lineCnt > 50) countAndWrite(root);
	}

	/* cost time counting */
	end_time = time((time_t *)NULL); // record end time
	cost_time = difftime(end_time, start_time); // total time cost
	printf("\n\n---- TIME COST FOR BUILDING ----\n");
	printf("START TIME: %d\n", start_time);
	printf("END TIME: %d\n", end_time);
	printf("COST TIME: %d\n", cost_time);
	printf("--------------------------------\n");
	printf("TOTAL NODE NUMBER: %d\n", nid);
	//printf("TOTAL MEMEORY USAGE: %f\n", MEMUSAGE);
	printf("--------------------------------\n");

	if(flag == 1){
		// test mode
		printf("#################\n");
		printf("    TEST MODE    \n");
		printf("#################\n");
		printf("tree generalizing by level 3...\n");
		generalize(root, 1, -99); // -99 is a meaningless value.
		printf("DONE!\n");
		interface(root);

		printf("\n@ print out the tree by preorder & inorder...\n");
		writeOut(root);
	}
	else{
		// query mode
		interface(root);
		
		printf("\n@ print out the tree by preorder & inorder...\n");
		writeOut(root);
	}

	printf("exit system...\n");
	return 0;
}

