#include "astlib.h"
/*
	# Note: 
		a. insert only work when the root exist.
		b. maybe rebuild many subtree, and result in overflow memory
	@ Agroithm:
		if curnode isn't NULL then 
				check if exfile exist or not
					if exist, rebuild
				record curnode's child
				check if child exist or not
					if it's not exist, 
						indicate there are not children. insert the current slope below this curnode.
					else, 
						indicate there are some children. find the matched node and update node's info.
						however, at last , if there are no matched children, 
						just insert current slopes at the tail of this generation.

	@ external thinking:
		when should the subtree be write out? after insert the slopes of OBS 
		how to write out a subtree? writeOut()
		how to rebuild the external tree? rebuild()

*/
void insert(STNODE *curnode, int obs, int *slopes, int pos, int slpnum){
	OBS *obslist;
	STNODE *chdnode;
	int mark = 0;
	if(curnode && (pos <= slpnum)){
		if(curnode->exfile) rebuild(curnode->exfile);
		chdnode = curnode->child;
		if(chdnode == NULL){
			/* if child doesn't exist */
			if(*slopes != 99){
				chdnode = createNode(obs, 1, *slopes, pos);
			}
			else{
				chdnode = createNode(obs, 2, *slopes, pos);
			}
			curnode->child = chdnode;
			slopes++;
			pos++;
			/*
			if(*slopes <= 90 && *slopes >= -90){
				chdnode = createNode(obs, 1, *slopes, pos);
				curnode->child = chdnode;
				slopes++;
				pos++;
				insert(chdnode, obs, slopes, pos, slpnum);
			}
			else if(*slopes == 99){
				chdnode = createNode(obs, 2, *slopes, pos);
				curnode->child = chdnode;
			}
			else{}
			*/
		}
		else{
			/* if child exists */
			// trace in this generation to find the matched node
			while(chdnode->sibling != NULL){
				if(chdnode->slope == *slopes){
					// found the matched node!
					/* update information of this node */
					chdnode->freq++;
					// update obslist
					obslist = chdnode->head;
					while(obslist->next != NULL) obslist = obslist->next;
					obslist->next = createObs(obs, pos, 0);
					/**********/
					slopes++;
					pos++;
					mark = 1;
					break;
				}
				else chdnode = chdnode->sibling; // doesn't find the matched node
			}
			if(mark == 0){ // no match in children
				if(chdnode->slope == *slopes){
					// check the last child in this generation
					/* update obslist of this node */
					chdnode->freq++;
					obslist = chdnode->head;
					while(obslist->next != NULL) obslist = obslist->next;
					obslist->next = createObs(obs, pos, 0);
					/**********/
					slopes++;
					pos++;
					mark = 1;
				}
				else{
					if(*slopes != 99){
						chdnode->sibling = createNode(obs, 1, *slopes, pos);
					}
					else{
						chdnode->sibling = createNode(obs, 2, *slopes, pos);
					}
					slopes++;
					pos++;
					chdnode = chdnode->sibling;
				}
			}
			insert(chdnode, obs, slopes, pos, slpnum);
		}
	}
}

