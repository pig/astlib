/*
	! Note:
	@ Algorithm:
		rebuild():
		create a inorder list, save nid & index like array
		create a preorder list, save nid & preorder like array & other info. of original node
		build tree!!
		return the root's address of subtree

		buildTree():
		pick current node from preorder list & increase preIndex
		if this node has no children then return
		else find the index of this node in inorder list
		using index in inorder list, construct left and right subtree

		pickAndCreateNode():
		accroding to the preorder linklist, read the necessary information which from is a string
		create a node space, split the info. string, save each info. into the created node.

		searchIndex():
		find the node's index in inorder linklist
*/

#include "astlib.h"

STNODE *rebuild(char *filename){
	STNODE *subroot;
	FILE *fptr;
	char *temp, *ch;
	/* declear for dynamic array structure */
	NODEARRAY *inorder, *curint, *newint;
	NODEARRAY *preorder, *curnode, *newnode;
	int inIndex = 0, preIndex = 0;
	int nid, type, slope, freq;
	/* open file */
	fptr = fopen(filename, "r");
	if(fptr == NULL){
		printf("[MSG]: opening file failed.");
		return NULL;
	}
	/* read file data */
	while(fgets(temp, BUFSIZE, fptr)){
		if(*temp == '$'){
			/* create inorder list */
			// save into inorder
			newint = (NODEARRAY *)malloc(sizeof(NODEARRAY));
			newint->nid = atoi(temp);
			newint->other = NULL;
			newint->index = inIndex;
			newint->next = NULL;
			// linking
			if(inIndex == 0){
				curint = newint;
				inorder = curint;
			}
			else{
				curint->next = newint;
				curint = curint->next;
			}
			inIndex++;
		}
		else{
			/* create preorder list */
			// get each line & create STNODE array
			// split temp
			ch = strtok(temp, " \n");
			nid = atoi(ch);
			// save each data
			newnode = (NODEARRAY *)malloc(sizeof(NODEARRAY));
			newnode->nid = nid;
			newnode->other = temp;
			newnode->index = preIndex;
			newnode->next = NULL;
			// linking
			if(preIndex == 0){
				curnode = newnode;
				preorder = curnode;
			}
			else{
				curnode->next = newnode;
				curnode = curnode->next;
			}
			preIndex++;
		}
	}
	/* let's build tree */
	subroot = buildTree(inorder, preorder, 0, inIndex-1);
	return subroot;
}

STNODE *buildTree(NODEARRAY *inorder, NODEARRAY *preorder, int inHead, int inTail){
	static int preIndex = 0;
	STNODE *stnode;
	int inIndex;

	if(inHead > inTail) return NULL;
	/* pick current node from preorder list & increase preIndex */
	stnode = pickAndCreateNode(preorder, preIndex);
	preIndex++;
	/* if this node has no children then return */
	if(inHead == inTail) return stnode;
	/* else find the index of this node in inorder list */
	inIndex = searchIndex(inorder, stnode->nid);
	/* using index in inorder list, construct left and right subtree */
	stnode->child = buildTree(inorder, preorder, inHead, inIndex-1);
	stnode->sibling = buildTree(inorder, preorder, inIndex+1, inTail);


	return stnode;
}
STNODE *pickAndCreateNode(NODEARRAY *preorder, int preIndex){
	STNODE *newnode;
	OBS *newobs, *curobs;
	char *ch;
	int id, pos, mark;
	int markObsHead = 0;
	int i = 0;

	while(i != preIndex){
		preorder = preorder->next;
		i++;
	}
	// split other string & save data into newnode which is created
	newnode = createNode(0, 0, 0, 0);
	// save nid
	ch = strtok(preorder->other, " \n");
	newnode->nid = atoi(ch);
	// save type
	ch = strtok(NULL, " \n");
	newnode->type = atoi(ch);
	// save slope
	ch = strtok(NULL, " \n");
	newnode->slope = atoi(ch);
	// save freq
	ch = strtok(NULL, " \n");
	newnode->freq = atoi(ch);
	// save exfile
	ch = strtok(NULL, " \n");
	newnode->exfile = ch;
	// save obslist head address
	ch = strtok(NULL, " \n");
	//*(newnode->head) = atoi(ch);
	//fprintf(newnode->head, "%d", atoi(ch));
	//sscanf(ch, "%d", newnode->head);
	sprintf(ch, "%d", newnode->head);
	/*
	while((ch = strtok(NULL, " \n"))){
		id = atoi(ch);
		ch = strtok(NULL, " \n");
		pos = atoi(ch);
		ch = strtok(NULL, " \n");
		mark = atoi(ch);
		newobs = createObs(id, pos, mark);

		if(markObsHead == 0){
			newnode->head = newobs;
			markObsHead = 1;
		}
		else{
			curobs = newnode->head;
			while(curobs->next) curobs = curobs->next;
			curobs->next = newobs;
		}
	}
	*/
	return newnode;
}
int searchIndex(NODEARRAY *inorder, int keyNid){
	while(inorder->nid != keyNid) inorder = inorder->next;
	return inorder->index;
}
