#include "astlib.h"

void interface(STNODE *root){
	char temp[BUFSIZE], *cmd;
	int head, tail;
	/* input interface */
	printf("\x1b[1;32m# SIMPLE SHELL #\x1b[m\n");
	printf("\x1b[1;32m# m1: Part Match.              Ex: $ m1 [query string]\x1b[m\n");
	printf("\x1b[1;32m# m2: FULL Match.              Ex: $ m2 [query string]\x1b[m\n");
	printf("\x1b[1;32m# m3: Match by head and tail.  Ex: $ m3 [head slope] [tail slope]\x1b[m\n");
	printf("\x1b[1;32m# help: show the usage of all query commands\x1b[m\n");
	printf("\x1b[1;32m# exit: exit this program\x1b[m\n");
	while(1){
		memset(temp, '\0', sizeof(temp));
		printf("\x1b[1;33m$ \x1b[m");
		fgets(temp, BUFSIZE, stdin);
		cmd = strtok(temp, " \n");
		if(strcmp(cmd, "m1") == 0){
			cmd = strtok(NULL, "\n");
			printf("\x1b[1;32m# [Query] Part Match \x1b[m\n");
			printf("\x1b[1;30m# your query: %s \x1b[m\n", cmd);
			queryPartMatch(root, cmd);
		}
		else if(strcmp(cmd, "m2") == 0){
			cmd = strtok(NULL, "\n");
			printf("\x1b[1;32m# [Query] Full Match\x1b[m\n");
			printf("\x1b[1;30m# your query: %s\x1b[m\n", cmd);
			queryFullMatch(root, cmd);

		}
		else if(strcmp(cmd, "m3") == 0){
			cmd = strtok(NULL, " ");
			head = atoi(cmd);
			cmd = strtok(NULL, "\n");
			tail = atoi(cmd);
			printf("\x1b[1;32m# [Query] Head Tail Match\x1b[m\n");
			printf("\x1b[1;30m# your query: head = %d, tail = %d\x1b[m\n", head, tail);
			queryHeadTailMatch(root, head, tail);
		}
		else if(strcmp(cmd, "help") == 0){
			printf("\t@ m1: Part Match.              Ex: $ m1 [query string]\n");
			printf("\t@ m2: FULL Match.              Ex: $ m2 [query string]\n");
			printf("\t@ m3: Match by head and tail.  Ex: $ m3 [head slope] [tail slope]\n");
			printf("\t@ help: show the usage of all query commands\n");
			printf("\t@ exit: exit this program\n");
		}
		else if(strcmp(cmd, "exit") == 0) break;
		else printf("\x1b[1;31m# [MSG] I don't know the command - %s.\x1b[m\n", cmd);
	}
}
