# Makefile for astronomy suffix tree library
CC = gcc

# Include path and Library path
#INC = -I /usr/include -I ${NAPATECH_INC}
#LIB = -L /usr/lib -L ${NAPATECH_LIB}
INC = 
LIB = 

CFLAG = -Wall ${INC} -g
LDFLAG = -lm -g

KERNAL_OBJ = main.o insertV2.o tools.o queryMatch.o interface.o
# TOOL_OBJ = init.o createObs.o createNode.o formated.o nodeCnt.o
EXTERNAL_OBJ = writeOut.o rebuild.o countAndWrite.o
GENERALIZE_OBJ = generalize.o
# DEBUG_OBJ = debug.o
OBJ = ${KERNAL_OBJ} ${EXTERNAL_OBJ} ${GENERALIZE_OBJ}
EXE = yggdrasill

.PHONY: clean all
all : ${OBJ}
	${CC} -o ${EXE} ${LDFLAG} ${OBJ} ${LIB}

.SUFFIX: .c

%.o : %.c
	${CC} $< -c ${CFLAG}

clean :
	rm -f ${OBJ} ${EXE}

