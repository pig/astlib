/*
	! Note:
		stnode: point to current node in the tree

	@ Algorithm:
		## queryFullMatch():
		spilt each queried slopes into a array.
		trace the tree.
		if stnode->slope != slopes[], then
			check stnode->exfile, if exist then rebuild
			check stnode->sibling
		if stnode->slope == slopes[], then
			save the obslist
			check stnode->exfile, if exist then rebuild
			check stnode->child
		check pos to know whether matched slope sequence exist or not

		## queryPartMatch():
		
		## queryHeadTailMatch():

		## markByTail():
*/
#include "astlib.h"

void queryFullMatch(STNODE *stnode, char *query){
	int slopes[SLPMAXNUM];
	int cnt = 0, pos = 0, obsnum = 0;
	char *tmp;
	OBS *obslist;
	int level = 1; 	
	
	memset(slopes, -99, sizeof(slopes));
	/* split query & save each slopeNUM into slopes[] */
	tmp = strtok(query, " ");
	slopes[cnt] = atoi(tmp);
	printf("slopes[%d]: %d\n", cnt, slopes[cnt]);
	cnt++;
	while((tmp = strtok(NULL, " "))){
		slopes[cnt] = atoi(tmp);
		printf("slopes[%d]: %d\n", cnt, slopes[cnt]);
		cnt++;
	}
	slopes[cnt] = 99;
	printf("slopes[%d]: %d\n", cnt, slopes[cnt]);
	
	/* start trace the tree */
	pos = 0;
	if(stnode->exfile){
		// rebuild tree
		stnode = rebuild(stnode->exfile);
	}
	if(stnode->child) stnode = stnode->child;
	else{
		printf("[ERROR] the tree is empty!\n");
		exit(1);
	}
	while(stnode){
		if(stnode->slope != slopes[pos]){
			if(stnode->exfile){
				// rebuild tree
				stnode = rebuild(stnode->exfile);
			}
			stnode = stnode->sibling;
		}
		else{
			obslist = stnode->head;
			if(stnode->exfile){
				// rebuild tree
				stnode = rebuild(stnode->exfile);
			}
			stnode = stnode->child;
			pos++;
		}
	}
	pos--;
	printf("---- pos: %d, cnt: %d ----\n", pos, cnt);
	printf("");
	if(pos == cnt){
		// match!
		printf("# \x1b[1;34mMATCH!!\x1b[m OBS list:\n");
		while(obslist){
			//printf("** obslist[%d][%d]\n", obslist->id, obslist->pos);
			if(obslist->pos == cnt){
				printf("\t%d[%d]\n", obslist->id, obslist->pos);
				obsnum++;
			}
			obslist = obslist->next;
		}
		printf("\ttotal %d matched obs\n", obsnum);
	}
	else{
		// not match.
		printf("# \x1b[1;32mNO MATCH.\x1b[m\n");
	}
}
void queryPartMatch(STNODE *stnode, char *query){
	int slopes[SLPMAXNUM];
	int cnt = 0, pos = 0, obsnum = 0;
	char *tmp;
	OBS *obslist;

	memset(slopes, -99, sizeof(slopes));
	/* split query & save each slopeNUM into slopes[] */
	tmp = strtok(query, " ");
	slopes[cnt] = atoi(tmp);
	cnt++;
	while((tmp = strtok(NULL, " "))){
		slopes[cnt] = atoi(tmp);
		cnt++;
	}
	/* start trace the tree */
	pos = 0;
	if(stnode->exfile){
		// rebuild tree
		stnode = rebuild(stnode->exfile);
	}
	if(stnode->child) stnode = stnode->child;
	while(stnode){
		if(stnode->slope != slopes[pos]){
			if(stnode->exfile) stnode = rebuild(stnode->exfile);
			stnode = stnode->sibling;
		}
		else{
			obslist = stnode->head;
			if(stnode->exfile) stnode = rebuild(stnode->exfile);
			stnode = stnode->child;
			pos++;
		}
	}
	if(pos == cnt){
		// match!
		printf("# \x1b[1;34mMATCH!!\x1b[m OBS list:\n");
		while(obslist){
			printf("\t%d[%d]\n", obslist->id, obslist->pos);
			obslist = obslist->next;
			obsnum++;
		}
		printf("\ttotal %d matched obs\n", obsnum);
	}
	else{
		// not match.
		printf("# \x1b[1;32mNO MATCH.\x1b[m\n");
	}
}
void queryHeadTailMatch(STNODE *stnode, int head, int tail){
	OBS *headset = NULL, *headset_head, *newobs;
	OBS *tmp;
	int cnt = 0;

	if(stnode->exfile){
		// rebuild tree
		stnode = rebuild(stnode->exfile);
	}
	if(stnode->child) stnode = stnode->child;
	while(stnode){
		if(stnode->exfile){
			// rebuild tree
			stnode = rebuild(stnode->exfile);
		}
		// search the same slope in the 1st generation nodes.
		if(stnode->slope != head){
			// rebuild tree
			if(stnode->exfile){
				// rebuild tree
				stnode  = rebuild(stnode->exfile);
			}
			stnode = stnode->sibling;
		}
		else{
			tmp = stnode->head;
			// get the obs that it's pos is 0, since the slope is it's head.
			while(tmp){
				if(tmp->pos == 0){
					//printf("** tmp[%d][%d][%d]\n", tmp->id, tmp->pos, tmp->mark);
					if(headset == NULL){
						headset = createObs(tmp->id, tmp->pos, 0);
						headset_head = headset;
					}
					else{
						newobs = createObs(tmp->id, tmp->pos, 0);
						headset->next = newobs;
						headset = headset->next;
					}
				}
				tmp = tmp->next;
			}
			break;
		}
	}
	// get the matched obsSet
	if(stnode){
		markByTail(stnode, headset_head, tail);
		tmp = headset_head;
		headset = headset_head;
		while(headset){
			if(headset->mark == 1){
				printf("\t%d\n", headset->id);
				cnt++;
			}
			headset = headset->next;
		}
		if(cnt != 0) printf("# total %d match.\n", cnt);
		else printf("# NO MATCH. @tail(%d)\n", tail);
	}
	else printf("# NO MATCH. @head(%d)\n", head);

}

void markByTail(STNODE *curnode, OBS *headset, int tail){
	STNODE *child = NULL;
	OBS *curobs = NULL, *obslist = NULL;
	int matched = 0;
	if(curnode != NULL){
		if(curnode->exfile){
			// rebuild tree
			curnode = rebuild(curnode->exfile);
		}
		child = curnode->child;
		if(curnode->slope == tail){
			// check the next generation wheather the ending is exist or not.
			while(child){
				if(child->slope == 99){
					matched = 1;
					break;
				}
				else{
					if(child->exfile){
						child = rebuild(child->exfile);
					}
					child = child->sibling;
				}
			}
			if(matched){
				curobs = headset;
				while(curobs){
					obslist = child->head;
					while(obslist){
						if(curobs->id == obslist->id) curobs->mark = 1;
						obslist = obslist->next;
					}
					curobs = curobs->next;
				}
			}
		}
		markByTail(curnode->child, headset, tail);
		markByTail(curnode->sibling, headset, tail);
	}
}


