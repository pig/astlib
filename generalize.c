#include "astlib.h"

void generalize(STNODE *node, int level, int custom){
	/*
		according to level, generalize the sub-tree which root is node
		level 4 is default. when we builded a suffix tree, the tree is level 4.

		step:
			up-down merge
			left-right merge
	*/
	STNODE *child, *tmp, *leftChild, *rightChild;
	if(node){
		/* external file */
		// if(node->exfile) rebuild(node->exfile);
		
		printf("# curnode[%d][%d] #\n", node->nid, node->slope);
		changeValue(node, level, custom);
		/* check for up-down merge */
		/*
		   check every child of the node
		   if they have the same leveled value, call verticalMerge().
		*/
		printf("+ verticalMerge().\n");
		child = node->child;
		leftChild = NULL;
		while(child){
			if(changeValue(child, level, custom) == node->slope){
				/* external file */
				// if(child->exfile) rebuild(child->exfile);

				child = verticalMerge(node, child, leftChild);
				//child = node->child;
			}
			else{
				leftChild = child;
				child = child->sibling;
			}
		}

		/* check for left-right merge */
		// sort the children in this generation by slope
		printf("+ sort the generation\n");
		child = node->child;
		while(child){
			rightChild = child->sibling;	
			while(rightChild){
				//printf("-- c[%d]%d, rc[%d]%d ---\n", child->nid, child->slope, rightChild->nid, rightChild->slope);
				if(child->slope > rightChild->slope){
					// swap
					swap(child, rightChild);
				}
				rightChild = rightChild->sibling;
			}
			child = child->sibling;
		}
		
		/* debug - check the sorted list
		child = node->child;
		while(child){
			printf("-- (LIST)c[%d][%d]\n", child->nid, child->slope);
			child = child->sibling;
		}
		*/

		// because we have sort them berfore, we can make sure that we don't need to check the previous nodes after merging.
		printf("+ horizontalMerge().\n");
		child = node->child;
		if(child){
			printf("- child[%d]%d --> %x\n", child->nid, child->slope, child->sibling);
			while(child->sibling){
				rightChild = child->sibling;
				printf("-- c[%d][%d], rc[%d]%d ---\n", child->nid, child->slope, rightChild->nid, rightChild->slope);
				if(changeValue(child, level, custom) == changeValue(child->sibling, level, custom)){
					/* external file */
					// rightChild = child->sibling;
					// if(rightChild->exfile) rebuild(rightChild->exfile);
					// if(child->exfile) rebuild(child->exfile);
					
					child = horizontalMerge(child, child->sibling);
				}
				else{
					child = child->sibling;
				}
			}
		}
		// debug - check the sorted list
		child = node->child;
		while(child){
			printf("-- (GEN)c[%d][%d]\n", child->nid, child->slope);
			child = child->sibling;
		}
		

		printf("+ END: go to next node\n");
		/*****/
		generalize(node->sibling, level, custom);
		generalize(node->child, level, custom);
	}
}

int changeValue(STNODE *node, int level, int custom){
	int value = 100; // 100 is meaningless value, indicate the function have some errors.
	
	if(level == 1) custom = 45;
	if(level == 2) custom = 15;
	if(level == 3) custom = 5;
	if(level == 5) custom = custom;

	if(node->slope > 0){
		if(node->slope != 99){
			node->slope = (node->slope/custom)*custom+(custom/2);
			value = node->slope;
		}
		else value = 99;
	}
	else if(node->slope < 0){
		if(node->slope != -99){
			node->slope = (node->slope/custom)*custom-(custom/2);
			value = node->slope;
		}
		else value = -99;
	}
	else{ /* do nothing */ }

	return value;
}

STNODE *verticalMerge(STNODE *dst, STNODE *src, STNODE *left){
	/*
		# algorithm:
		
		# dst: parent, up
		# src: child, down
		# left: child's left sibling
		# return: the node which should be check next
	*/
	STNODE *tmp, *right;
	right = src->sibling;	
	
	printf("- exe VM\n");
	/** merge information **/
	nodeInfoMerge(dst, src);
	/***********************/

	/** update links setting **/
	if(src){
		if(left){
			// case 2 & 3
			left->sibling = src->sibling;
			tmp = left;
			while(tmp->sibling){
				tmp = tmp->sibling;
			}
			tmp->sibling = src->child;
			if(!right){
				right = left->sibling;
			}
		}
		else{
			// case 1.1 & 1.2
			dst->child = src->sibling;
			tmp = dst->child;
			if(tmp){
				// there are other children
				while(tmp->sibling){
					tmp = tmp->sibling;
				}
				tmp->sibling = src->child;
			}
			else{
				// link grandson
				dst->child = src->child;
			}
		}
		
		
		free(src);
		return right;
	}
}

STNODE *horizontalMerge(STNODE *dst, STNODE *src){
	/*
		# algorithm:
		
		# dst: left
		# src: right
		# return: the dst which has been reset the links.
	*/

	STNODE *tmp;

	printf("- exe HM\n");
	/** merge information **/
	nodeInfoMerge(dst, src);
	/***********************/

	/** update links setting **/
	if(src){
		dst->sibling = src->sibling;
		tmp = dst->child;
		if(tmp){
			while(tmp->sibling){
				tmp = tmp->sibling;
			}
			tmp->sibling = src->child;
		}
		else{
			dst->child = src->child;
		}
	}
	
	free(src);
	return dst;
}

void swap(STNODE *r, STNODE *s){
	STNODE *t;
	
	t = (STNODE *)malloc(sizeof(STNODE));
	// t = r
	t->nid = r->nid;
	t->type = r->type;
	t->slope = r->slope;
	t->freq = r->freq;
	t->exfile = r->exfile;
	t->head = r->head;
	t->child = r->child;
	//t->sibling = r->sibling;
	
	// r = s
	r->nid = s->nid;
	r->type = s->type;
	r->slope = s->slope;
	r->freq = s->freq;
	r->exfile = s->exfile;
	r->head = s->head;
	r->child = s->child;
	//r->sibling = s->sibling;

	// s = t
	s->nid = t->nid;
	s->type = t->type;
	s->slope = t->slope;
	s->freq = t->freq;
	s->exfile = t->exfile;
	s->head = t->head;
	s->child = t->child;
	//s->sibling = t->sibling;

}

void nodeInfoMerge(STNODE *dst, STNODE *src){
	OBS *head;
	printf("-- nodeInfoMerge \n");
	dst->freq = dst->freq + src->freq;
	head = dst->head;
	while(head->next){
		head = head->next;
	}
	head->next = src->head; // simple merge. actually, we should filt the same obs.
	printf("---- info: nid=%d, slope=%d, freq=%d\n", dst->nid, dst->slope, dst->freq);

}
